const path = require('path');
const babiliPlugin = require('babili-webpack-plugin');
const extractTextPlugin = require('extract-text-webpack-plugin'); 
const optimizeCSSAssetPlugin = require('optimize-css-assets-webpack-plugin');
const htmlWebpackPlugin = require('html-webpack-plugin');

const webpack = require('webpack');
const plugins = [];


// Definindo ENDERECO das APIS externas
let SERVICE_URL = JSON.stringify("http://localhost:3000/");


//Criando a index.html
plugins.push(new htmlWebpackPlugin({
	hash: true,
	minify: {
		html5: true,
		collapseWhitespace: true,
		removeComments: true
	},
	filename: 'index.html',
	template: `${__dirname}/main.html`
}));



// Gerando um CSS minificado nome do arquivo que sera gerado
plugins.push(new extractTextPlugin('styles.css')); 

// Compartilhando o Jquery entre os modulos
plugins.push(new webpack.ProvidePlugin({
	'$': 'jquery/dist/jquery.js',
	'jQuery': 'jquery/dist/jquery.js',
	'JQuery': 'jquery/dist/jquery.js'
}));

// Separando o codugo da aplicacao das bibliotecas de terceiros
plugins.push(new webpack.optimize.CommonsChunkPlugin({
	name: 'vendor',
	filename: 'vendor.bundle.js'
}));



// Plugins para poducao
if(process.env.NODE_ENV == "production") {
 
	SERVICE_URL = JSON.stringify("http://novo-endereco-da-api-para-producao/");

	plugins.push(new webpack.optimize.ModuleConcatenationPlugin());

	// Plugin babili que liga o babel(tranpiler) com o webpack
	plugins.push(new babiliPlugin());

	// Plugin de pre-processamento do cs
	plugins.push(new optimizeCSSAssetPlugin({
		cssProcessor: require('cssnano'),
		cssProcessorOptions: {
			discardComments: {
				removeAll: true,
			}
		},
		canPrint: true
	}));


}

//define a const SERVICE_URL
plugins.push(new webpack.DefinePlugin({ SERVICE_URL }))

// Pontos de entrada/saida da aplicacao
// Regras (rules) que inclui otimizacao css,js, e fonts
module.exports = {
	entry: {
        app: './app-src/app.js',
        vendor: ['jquery', 'bootstrap', 'reflect-metadata']
	},
	output: {
		filename: 'bundle.js',
		path: path.resolve(__dirname, 'dist')
	},

	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /node_modules/,
				use: {
					loader: 'babel-loader'
				}

			},
			{
				test: /\.css$/,
				use: extractTextPlugin.extract({
					fallback: 'style-loader',
					use: 'css-loader'
				})
			},
			{ 
                test: /\.(woff|woff2)(\?v=\d+\.\d+\.\d+)?$/, 
                loader: 'url-loader?limit=10000&mimetype=application/font-woff' 
            },
            { 
                test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/, 
                loader: 'url-loader?limit=10000&mimetype=application/octet-stream'
            },
            { 
                test: /\.eot(\?v=\d+\.\d+\.\d+)?$/, 
                loader: 'file-loader' 
            },
            { 
                test: /\.svg(\?v=\d+\.\d+\.\d+)?$/, 
                loader: 'url-loader?limit=10000&mimetype=image/svg+xml' 
            }      
		]
	},

	plugins

};