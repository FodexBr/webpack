import 'jquery/dist/jquery.js';

import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/css/bootstrap-theme.min.css';
import 'bootstrap/js/modal.js';
import '../css/meu-estilo.css';


import { NegociacaoController } from './controllers/NegociacaoController.js';
import { Negociacao } from './domain/index.js';

const controller = new NegociacaoController();
const negociacao = new Negociacao(new Date(), 1, 200);
const headers = new Headers();
headers.set('Content-Type', 'application/json');
const body = JSON.stringify(negociacao);
const method = 'POST';

const config = { 
    method,
    headers,
    body 
};

//$("h1").on('click', () => alert("Teste"));

fetch(`${SERVICE_URL}negociacoes`, config)
    .then(() => console.log('Dado enviado com sucesso'));