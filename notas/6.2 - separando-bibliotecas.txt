** Separando o codigo e as bibliotecas de terceitos em bundles separados

	// Codigo nativo(Nosso codigo, de logica e regras de negocio),
	ficaram em um bundle.js

	// Os codigos de terceiros, por exemplo, jquery, loadash,
	underscore, ficaram em "vendor.bundle.js"

	//Configura o Plugin
	plugins.push(new webpack.optimize.CommonsChunkPlugin({
		name: 'vendor', // apelido
		filename: 'vendor.bundle.js'
	}))

	// Separa os pontos de entrada
	module.exports = {
		entry: {
			app: './app-src/app.js',
			vendor: [
				'bootstrap',
				'jquery',
				'reflect-metadata'
			]
		},
	};


	// Importando os codigos separados
	<script src="dist/vendor.bundle.js"></script>
    <script src="dist/bundle.js"></script>